﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Axel.MiniBicks.Data.Models
{
    public class Absence
    {
        [Key] 
        public int AbsenceId { get; set; }

        public string Libelle { get; set; }
    }
}
