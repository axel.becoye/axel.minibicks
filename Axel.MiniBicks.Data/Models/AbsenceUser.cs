﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Axel.MiniBicks.Data.Models
{
    public class AbsenceUser
    {
        [Key]
        public int AbsenceUserId { get; set; }
        public DateTime DateDebut { get; set; }
        public DateTime DateFin { get; set; }
        public string Justificatif { get; set; }

        [ForeignKey("User")]
        public int Userid { get; set; }
        public User User { get; set; }

        [ForeignKey("Absence")]
        public int AbsenceId { get; set; }
        public Absence Absence { get; set; }
    }
}
