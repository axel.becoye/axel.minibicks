﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;
using DbContext = Microsoft.EntityFrameworkCore.DbContext;

namespace Axel.MiniBicks.Data
{
    public class BickContext: DbContext
    {
        public DbSet<Models.User> Users { get; set; }
        public DbSet<Models.Role> Roles { get; set; }
        public DbSet<Models.Absence> Absences { get; set; }
        public DbSet<Models.AbsenceUser> AbsenceUsers { get; set; }
        public DbSet<Models.Remboursement> Remboursements { get; set; }
        public DbSet<Models.RemboursementUser> RemboursementUsers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite(@"Data Source=C:\Users\Friz\Documents\CSharp\Axel.MiniBicks\Axel.MiniBicks.Data\MiniBicks.db");
    }
}
