﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Axel.MiniBicks.Data;
using Axel.MiniBicks.Data.Models;

namespace Axel.MiniBicks.Lib
{
    public class UserFactory
    {
        BickContext db = new BickContext();

        public List<User> GetAllUsers() => db.Users.ToList();
    }
}
