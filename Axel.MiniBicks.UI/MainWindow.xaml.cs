﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Axel.MiniBicks.Data;
using Axel.MiniBicks.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace MiniBicks
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        User modelUser = new User();
        Remboursement modelRefund = new Remboursement();
        Absence modelLeave = new Absence();
        RemboursementUser modelRefundUser = new RemboursementUser();
        AbsenceUser modelLeaveUser = new AbsenceUser();
        public ObservableCollection<User> UserCollection { get; set; }
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window1_Load(object sender, EventArgs e)
        {
            Clear();
            LoadData();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Clear();
            LoadData();
        }

        private void Clear()
        {
            txtFirstName.Text = txtLastName.Text = txtAddress.Text = "";
            btnSave.Content = LeaveBtnSave.Content = RefundBtnSave.Content = "Save";
            btnDelete.IsEnabled = LeaveBtnDelete.IsEnabled = RefundBtnDelete.IsEnabled = false;
            comboSuperieur.SelectedIndex = comboRefun.SelectedIndex = comboRefunUser.SelectedIndex = comboLeave.SelectedIndex = comboLeaveUser.SelectedIndex = comboRole.SelectedIndex = -1;
            RefundStartDate.SelectedDate = RefundEndDate.SelectedDate = LeaveStartDate.SelectedDate = LeaveEndDate.SelectedDate = null;
            modelUser.UserId = modelRefundUser.UserId = modelRefundUser.RemboursementUserId =
                modelRefundUser.RemboursementUserId = modelLeaveUser.Userid =
                    modelLeaveUser.AbsenceId = modelLeaveUser.AbsenceUserId = 0;

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            modelUser.FirstName = txtFirstName.Text.Trim();
            modelUser.LastName = txtLastName.Text.Trim();
            modelUser.Address = txtAddress.Text.Trim();
            if (comboSuperieur != null)
            {
                User superieur = comboSuperieur.SelectionBoxItem as User;
                if (superieur != null)
                {
                    modelUser.SuperieurId = superieur.UserId;
                    modelUser.Superieur = superieur;
                }
            }
            if (comboRole != null)
            {
                Role role = comboRole.SelectionBoxItem as Role;
                if (role != null)
                {
                    modelUser.RoleId = role.RoleId;
                    modelUser.Role = role;
                }
            }
            using (var db = new BickContext())
            {
                if (modelUser.UserId == 0) //In case of Insert
                {
                    db.Entry(modelUser).State = EntityState.Added;
                }
                else
                {
                    db.Entry(modelUser).State = EntityState.Modified;
                }

                db.SaveChanges();
            }

            Clear();
            LoadData();
            MessageBox.Show("Submitted Successfully");
        }

        void LoadData()
        {
            using (var db = new BickContext())
            {
                //User Management
                dgvUser.ItemsSource = db.Users.ToList();
                comboSuperieur.ItemsSource = db.Users.ToList();
                comboSuperieur.DisplayMemberPath = "FirstName";
                comboSuperieur.SelectedValuePath = "UserId";
                comboRole.ItemsSource = db.Roles.ToList();
                comboRole.DisplayMemberPath = "Libelle";
                comboRole.SelectedValuePath = "RoleId";

                //Refund Management
                dgvRefund.ItemsSource = db.RemboursementUsers.ToList();
                comboRefun.ItemsSource = db.Remboursements.ToList();
                comboRefun.DisplayMemberPath = "Libelle";
                comboRefun.SelectedValuePath = "AbsenceId";
                comboRefunUser.ItemsSource = db.Users.ToList();
                comboRefunUser.DisplayMemberPath = "FirstName";
                comboRefunUser.SelectedValuePath = "UserId";

                //Leave Management
                dgvLeave.ItemsSource = db.AbsenceUsers.ToList();
                comboLeave.ItemsSource = db.Absences.ToList();
                comboLeave.DisplayMemberPath = "Libelle";
                comboLeave.SelectedValuePath = "AbsenceId";
                comboLeaveUser.ItemsSource = db.Users.ToList();
                comboLeaveUser.DisplayMemberPath = "FirstName";
                comboLeaveUser.SelectedValuePath = "UserId";
            }
        }

        private void dgvUser_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            using (var db = new BickContext())
            {
                User selectedUser = (User)dgvUser.SelectedItem;
                //User selectedUser = dgvUser.SelectedItem as User;
                if (selectedUser != null)
                {
                    int selectedUserId = selectedUser.UserId;
                    modelUser = db.Users.FirstOrDefault(x => x.UserId == selectedUserId);
                    if (selectedUser.Superieur != null)
                    {
                        comboSuperieur.SelectedItem = selectedUser.Superieur;
                    }
                    if (selectedUser.Role != null)
                    {
                        comboRole.SelectedItem = selectedUser.Role;
                    }

                    if (modelUser != null)
                    {
                        txtFirstName.Text = modelUser.FirstName;
                        txtLastName.Text = modelUser.LastName;
                        txtAddress.Text = modelUser.Address;
                    }
                }
            }

            btnSave.Content = "Update";
            btnDelete.IsEnabled = true;
        }

        private void RefundBtnBrowse_Click(object sender, RoutedEventArgs e)
        {
            Stream checkStream = null;
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Multiselect = false;
            dlg.Filter = "All Files | *.*";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                try
                {
                    string filename = dlg.FileName;
                    RefundBtnBrowse.Content = filename;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    throw;
                }
            }
        }

        private void LeaveBtnBrowse_Click(object sender, RoutedEventArgs e)
        {
            Stream checkStream = null;
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Multiselect = false;
            dlg.Filter = "All Files | *.*";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                try
                {
                    string filename = dlg.FileName;
                    LeaveBtnBrowse.Content = filename;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    throw;
                }
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete this record ?", "Message", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                using (var db = new BickContext())
                {
                    var entry = db.Entry(modelUser);
                    if (entry.State == EntityState.Detached)
                    {
                        db.Users.Attach(modelUser);
                        db.Users.Remove(modelUser);
                        db.SaveChanges();
                        LoadData();
                        Clear();
                        MessageBox.Show("Deleted Successfully");
                    }
                }
            }
        }

        private void RefundBtnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete this record ?", "Message", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                using (var db = new BickContext())
                {
                    var entry = db.Entry(modelRefundUser);
                    if (entry.State == EntityState.Detached)
                    {
                        db.RemboursementUsers.Attach(modelRefundUser);
                        db.RemboursementUsers.Remove(modelRefundUser);
                        db.SaveChanges();
                        LoadData();
                        Clear();
                        MessageBox.Show("Deleted Successfully");
                    }
                }
            }
        }

        private void LeaveBtnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete this record ?", "Message", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                using (var db = new BickContext())
                {
                    var entry = db.Entry(modelLeaveUser);
                    if (entry.State == EntityState.Detached)
                    {
                        db.AbsenceUsers.Attach(modelLeaveUser);
                        db.AbsenceUsers.Remove(modelLeaveUser);
                        db.SaveChanges();
                        LoadData();
                        Clear();
                        MessageBox.Show("Deleted Successfully");
                    }
                }
            }
        }

        private void RefundBtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Clear();
            LoadData();
        }

        private void LeaveBtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Clear();
            LoadData();
        }

        private void RefundBtnSave_Click(object sender, RoutedEventArgs e)
        {
            User refundUser = comboRefunUser.SelectionBoxItem as User;
            Remboursement refundType = comboRefun.SelectionBoxItem as Remboursement;
            modelRefundUser.User = refundUser;
            if (refundUser != null) modelRefundUser.UserId = refundUser.UserId;
            modelRefundUser.DateDebut = Convert.ToDateTime(RefundStartDate.Text);
            modelRefundUser.DateFin = Convert.ToDateTime(RefundEndDate.Text);
            modelRefundUser.Remboursement = refundType;
            if (refundType != null) modelRefundUser.RemboursementId = refundType.RemboursementId;

            using (var db = new BickContext())
            {
                if (modelRefundUser.RemboursementUserId == 0) //In case of Insert
                {
                    db.Entry(modelRefundUser).State = EntityState.Added;
                }
                else
                {
                    db.Entry(modelRefundUser).State = EntityState.Modified;
                }

                db.SaveChanges();
            }
            Clear();
            LoadData();
            MessageBox.Show("Submitted Successfully");
        }

        private void LeaveBtnSave_Click(object sender, RoutedEventArgs e)
        {
            User leaveUser = comboLeaveUser.SelectionBoxItem as User;
            Absence leaveType = comboLeave.SelectionBoxItem as Absence;
            modelLeaveUser.User = leaveUser;
            if (leaveUser != null) modelLeaveUser.Userid = leaveUser.UserId;
            modelLeaveUser.DateDebut = Convert.ToDateTime(LeaveStartDate.Text);
            modelLeaveUser.DateFin = Convert.ToDateTime(LeaveEndDate.Text);
            modelLeaveUser.Absence = leaveType;
            if (leaveType != null) modelLeaveUser.AbsenceId = leaveType.AbsenceId;

            using (var db = new BickContext())
            {
                if (modelLeaveUser.AbsenceUserId == 0) //In case of Insert
                {
                    db.Entry(modelLeaveUser).State = EntityState.Added;
                }
                else
                {
                    db.Entry(modelLeaveUser).State = EntityState.Modified;
                }

                db.SaveChanges();
            }
            Clear();
            LoadData();
            MessageBox.Show("Submitted Successfully");
        }

        private void dgvRefund_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            using (var db = new BickContext())
            {
                RemboursementUser selectedRefund = (RemboursementUser)dgvRefund.SelectedItem;
                if (selectedRefund != null)
                {
                    int selectedRefundId = selectedRefund.RemboursementUserId;
                    modelRefundUser = db.RemboursementUsers.FirstOrDefault(x => x.RemboursementUserId == selectedRefundId);
                    if (selectedRefund.User != null)
                    {
                        comboRefunUser.SelectedItem = selectedRefund.User;
                    }
                    if (selectedRefund.Remboursement != null)
                    {
                        comboRefun.SelectedItem = selectedRefund.Remboursement;
                    }

                    RefundStartDate.SelectedDate = selectedRefund.DateDebut;
                    RefundEndDate.SelectedDate = selectedRefund.DateFin;
                }
            }

            RefundBtnSave.Content = "Update";
            RefundBtnDelete.IsEnabled = true;
        }

        private void dgvLeave_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            using (var db = new BickContext())
            {
                AbsenceUser selectedLeave = (AbsenceUser)dgvLeave.SelectedItem;
                if (selectedLeave != null)
                {
                    int selectedLeaveId = selectedLeave.AbsenceUserId;
                    modelLeaveUser = db.AbsenceUsers.FirstOrDefault(x => x.AbsenceUserId == selectedLeaveId);
                    if (selectedLeave.User != null)
                    {
                        comboLeaveUser.SelectedItem = selectedLeave.User;
                    }
                    if (selectedLeave.Absence != null)
                    {
                        comboLeave.SelectedItem = selectedLeave.Absence;
                    }

                    LeaveStartDate.SelectedDate = selectedLeave.DateDebut;
                    LeaveEndDate.SelectedDate = selectedLeave.DateFin;
                }
            }

            LeaveBtnSave.Content = "Update";
            LeaveBtnDelete.IsEnabled = true;
        }
    }
}
